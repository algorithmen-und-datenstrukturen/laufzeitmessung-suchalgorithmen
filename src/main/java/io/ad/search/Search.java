package io.ad.search;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Collections;
import java.util.List;

public class Search {

	public static void main(String[] args) {

		final int numberOfRuns = 30;
		final int numberOfCorrections = 3;

		for (int algoType = 0; algoType < 2; algoType++) {
			System.out.println("A=" + (algoType == 0 ? "Linear" : "Binary"));
			for (int listType = 0; listType < 2; listType++) {
				System.out.println("\tL=" + (listType == 0 ? "ArrayList" : "LinkedList"));
				for (int numbers = 2000000; numbers < 10000001; numbers += 2000000) {

					// WarmUP
					List<Integer> warmUpList = new ArrayList<>();
					for (int i = 0; i < 10000000; i++) {
						warmUpList.add(i);
					}

					// ResultLists
					List<Long> result = new ArrayList<>();

					long start = 0;
					long stop = 0;

					for (int a = 0; a < numberOfRuns; a++) {
						List<Integer> list = createList(numbers, listType);

						if (algoType == 0) {
							start = System.nanoTime();
							linearSearchFor(list, numbers);
							stop = System.nanoTime();
						} else {
							start = System.nanoTime();
							Collections.binarySearch(list, numbers);
							stop = System.nanoTime();
						}

						result.add(stop - start);
					}

					cleanResults(result, numberOfCorrections);
					System.out.print("\t\tN=" + numbers+ "-> ");
					
					
					// System.out.println(listToString(result));
					System.out.println("Average= " + calculateAverage(result) + " ns\n");

				}
			}
		}
	}

	private static List<Long> cleanResults(List<Long> list, int corrections) {

		for (int i = 0; i < corrections; i++) {
			long highestValue = Collections.max(list);
			long lowestValue = Collections.min(list);
			list.remove(highestValue);
			list.remove(lowestValue);
		}

		return list;
	}

	private static long calculateAverage(List<Long> list) {
		long average = 0;
		for (long i : list) {
			average += i;
		}
		return average / list.size();
	}

	private static String listToString(List<Long> list) {
		StringBuilder sb = new StringBuilder();
		for (long i : list) {
			sb.append(i);
			sb.append("\n");
		}
		return sb.toString();
	}

	private static List<Integer> createList(int numbers, int listType) {
		List<Integer> list = null;
		if (listType == 0)
			list = new ArrayList<>();
		else
			list = new LinkedList<>();
		for (int i = 0; i < numbers; i++) {
			list.add(i);
		}
		return list;
	}
	
	public static int linearSearchFor(List<Integer> list, int n) {
		int pos = 0;
		
		for (Integer i : list) {
			if (i == n) {
				return pos;
			}
			pos++;
		}
		return -1;
	}

}
